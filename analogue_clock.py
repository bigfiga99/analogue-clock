import pygame, math
from datetime import datetime
from sys import exit

screen_size = [500, 500]
screen_center = screen_size[0]//2, screen_size[1]//2
hand_thickness = 5

pygame.init()
screen = pygame.display.set_mode(screen_size)
pygame.display.set_caption("Old Clock")
font = pygame.font.Font(pygame.font.get_default_font(), 25)

tick = math.cos(math.pi/30) + math.sin(math.pi/30) * 1j
tick5 = math.cos(math.pi/6) + math.sin(math.pi/6) * 1j
tick45 = math.cos(3*math.pi/2) + math.sin(3*math.pi/2) * 1j

def cipair(n):
    return int(n.real), int(n.imag)

def draw_markings(center, radius, length):
    x1, x2 = radius + 0j, radius + length + 0j
    for i in range(12):
        pygame.draw.line(screen, "Black", cipair(x1 + complex(*center)), cipair(x2 + complex(*center)), 5)
        x1 *= tick5
        x2 *= tick5

def draw_hands(center, radius, angle, thickness, colour):
    x1 = center[0] + center[1]*1j
    x2 = x1 + radius * (tick45 * tick ** angle)
    pygame.draw.line(screen, colour, cipair(x1), cipair(x2), thickness)

def draw_numbers(surface, font, center, radius):
    numbers = ["12", "3", "6", "9"]

    for i, number in enumerate(numbers):
        text = font.render(number, False, "Black")
        width, height = font.size(number)
        if i % 2 == 0:
            surface.blit(text, (center[0]-(width//2), (center[1]-radius)-(height//2)))
        else:
            surface.blit(text, ((center[0]+radius)-(width//2), center[1]-(height//2)))
            radius *= -1

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()

    screen.fill('Black')
    pygame.draw.circle(screen, 'White', (screen_center), 200)
    pygame.draw.circle(screen, 'Black', (screen_center), 10)
    draw_markings(screen_center, 170, 30)
    draw_numbers(screen, font, screen_center, 155)

    current_time = datetime.now().strftime("%I%M%S")
    hours = int(current_time[:2])
    minutes = int(current_time[2:4])
    seconds = int(current_time[4:])
    
    draw_hands(screen_center, 140, seconds, hand_thickness, "Pink")
    draw_hands(screen_center, 120, minutes, hand_thickness, "Magenta")
    draw_hands(screen_center, 100, hours/12 * 60, hand_thickness, "Red")

    pygame.display.update()    